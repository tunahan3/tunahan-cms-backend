<?php

require __DIR__ . '/default.php';

use Ufukyucel\TunahanCmsBackend\Dto\UserData;
use Ufukyucel\TunahanCmsBackend\Utils\TokenUtils;

class TestController {

    public function toString() : string{
        $tokenService = new TokenUtils();
        $userData = new UserData(
            id: 1,
            username: 'manlyak',
            mail: '',
            password: '',
            createdDate: '',
            updatedDate: '',
            roles: 0
        );
        return $tokenService->createToken($userData);
    }

}

$test = new TestController();
echo $test->toString()

?>