<?php

require __DIR__ . '/default.php';

use Ufukyucel\TunahanCmsBackend\Inc\Database;

$db = new Database();
$connection = $db->getConnection();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $requestBody = file_get_contents('php://input');
    $data = json_decode($requestBody);

    $siteUrl = $data->siteUrl;
    $siteTitle = $data->siteTitle;
    $siteDesc = $data->siteDesc;
    $siteKeyw = $data->siteKeyw;
    $siteEmail = $data->siteEmail;
    $siteState = $data->siteState;

    $sqlHas = 'SELECT * FROM settings';
    $queryHas = $connection->prepare($sqlHas);
    $queryHas->execute();
    $result = $queryHas->fetch(PDO::FETCH_ASSOC);
    if ($result) {
        $sql = 'UPDATE settings SET site_url = :site_url, site_title = :site_title, site_desc = :site_desc,
                site_keyw = :site_keyw, site_email = :site_email, site_state = :site_state';
    } else {
        $sql = 'INSERT INTO settings (site_url, site_title, site_desc, site_keyw, site_email, site_state)
                VALUES (:site_url, :site_title, :site_desc, :site_keyw, :site_email, :site_state)';
    }

    $query = $connection->prepare($sql);
    $query->bindParam(':site_url', $siteUrl);
    $query->bindParam(':site_title', $siteTitle);
    $query->bindParam(':site_desc', $siteDesc);
    $query->bindParam(':site_keyw', $siteKeyw);
    $query->bindParam(':site_email', $siteEmail);
    $query->bindParam(':site_state', $siteState);
    $query->execute();

    http_response_code(200);
    exit();
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $sql = 'SELECT * FROM settings';
    $query = $connection->prepare($sql);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);
    echo json_encode($result);
    exit();
}


?>