<?php

namespace Ufukyucel\TunahanCmsBackend\Utils;

use DateTime;
use Firebase\JWT\JWT;
use PDO;
use Ramsey\Uuid\Uuid;
use Ufukyucel\TunahanCmsBackend\Dto\UserData;
use Ufukyucel\TunahanCmsBackend\Inc\Database;

class TokenUtils {

    public function createToken(UserData $userData): string
    {

        $db = new Database();
        $connection = $db->getConnection();

        $hasToken = $this->VerifyToken($userData);
        if($hasToken){
            return $hasToken['token'];
        }

        $secretKey = Uuid::uuid4()->toString();
        $serverName = "tunadastan.com";
        $username = $userData->username;

        $created_at = date('Y-m-d H:i:s');
        $expire_at = date('Y-m-d H:i:s', strtotime('+1 day'));

        $claims = [
            'iat'  => $created_at,         // Issued at: time when the token was generated
            'iss'  => $serverName,                       // Issuer
            'nbf'  => $created_at,         // Not before
            'exp'  => $expire_at,                           // Expire
            'userName' => $username,
        ];

        $token = JWT::encode(
            $claims,
            $secretKey,
            'HS512'
        );

        $sql = 'INSERT INTO tokens (token, user_id, created_at, issuer, not_before, expire_at, username)
        VALUES (:token, :user_id, :created_at, :issuer, :not_before, :expire_at, :username)';
        $query = $connection->prepare($sql);
        $query->bindParam(':token', $token);
        $query->bindParam(':user_id', $userData->id);
        $query->bindParam(':created_at', $created_at);
        $query->bindParam(':issuer', $serverName);
        $query->bindParam(':not_before', $created_at);
        $query->bindParam(':expire_at', $expire_at);
        $query->bindParam(':username', $userData->username);
        $query->execute();

        return $token;
    }

    public function VerifyToken(UserData $userData) {

        $db = new Database();
        $connection = $db->getConnection();

        $query = $connection->prepare('SELECT * FROM tokens WHERE user_id = :user_id AND username = :username');
        $query->bindParam(':user_id', $userData->id);
        $query->bindParam(':username', $userData->username);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $currentDateTime = new DateTime();
        if($result && $result['expire_at'] > $currentDateTime){
            return false;
        }
        return $result;
    }

}

?>