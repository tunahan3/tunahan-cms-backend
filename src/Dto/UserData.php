<?php

namespace Ufukyucel\TunahanCmsBackend\Dto;
class UserData {

    public int $id;
    public string $username;
    public string $mail;
    public string $password;
    public string $createdDate;
    public string $updatedDate;
    public int $roles;

    public function __construct(int $id, string $username, string $mail, string $password, string $createdDate, string $updatedDate, int $roles) {
        $this->id = $id;
        $this->username = $username;
        $this->mail = $mail;
        $this->password = $password;
        $this->createdDate = $createdDate;
        $this->updatedDate = $updatedDate;
        $this->roles = $roles;
    }

}

?>