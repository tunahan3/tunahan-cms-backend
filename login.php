<?php

require __DIR__ . '/default.php';

use Ufukyucel\TunahanCmsBackend\Dto\UserData;
use Ufukyucel\TunahanCmsBackend\Inc\Database;
use Ufukyucel\TunahanCmsBackend\Utils\TokenUtils;

$db = new Database();
$connection = $db->getConnection();

$requestBody = file_get_contents('php://input');
$data = json_decode($requestBody);

$username = $data->username ?? '';
$password = $data->password ?? '';
$password = md5(sha1(sha1($password)));

$query = $connection->prepare('SELECT * FROM users WHERE username = :username AND password = :password');
$query->bindParam(':username', $username);
$query->bindParam(':password', $password);

$query->execute();

$result = $query->fetch(PDO::FETCH_ASSOC);

if($result){
    $userData = new UserData(
        id: $result['id'],
        username: $result['username'],
        mail: $result['email'],
        password: '',
        createdDate: $result['created_at'] ?? '',
        updatedDate: $result['updated_date'] ?? '',
        roles: $result['roles'] ?? 0
    );
    $tokenUtils = new TokenUtils();
    echo $tokenUtils->createToken($userData);
}else {
    http_response_code(401);
    echo 'Unauthorized';
    exit();
}

?>