<?php

namespace Ufukyucel\TunahanCmsBackend;

// Tüm isteklere izin vermek için
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Content-Type');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($className) {
    $namespacePrefix = 'Ufukyucel\\TunahanCmsBackend\\';
    $className = str_replace($namespacePrefix, '', $className);
    $className = str_replace('\\', '/', $className);
    $filePath = __DIR__ . '/src/' . $className . '.php';
    if (file_exists($filePath)) {
        require_once $filePath;
    }
});

require_once __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

?>